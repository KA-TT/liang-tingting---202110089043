package 幼儿园y;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
interface IParams {
    // 获取大班名额
    public int getBig();
    // 获取中班名额
    public int getMedium();   
    // 获取小班名额
    public int getSmall();       
    // 获取入学序列，例如 [1 2 2 3] 表示依次报名入学一名大班、中班、中班、小班学生
    public ArrayList<Integer> getPlanSignUp ();
}
interface ISignUp {
    // 打印输出结果
    public void print();
    // 检查是否有 stuType对应的班级名额
    // 如果没有剩余名额，请返回 false ，否则学生将报名进入该班级并返回 true
    public boolean addStudent(int stuType);
    // 解析命令行输入的参数（格式），如文档描述
    public static IParams parse() throws Exception {
    	    Scanner scanner = new Scanner(System.in);
	    String nextLine = scanner.nextLine();      	
	    String nextLine1 = scanner.nextLine();
	    String replace = nextLine1.replace("[", "").replace("]", "").replace(" ", "");	//用replace替换字符或字符串
	    List<String> strings = Arrays.asList(replace.split(","));
	    int big = 0;
	    int medium = 0;
	    int small = 0;
	    ArrayList<Integer> planSignUp = new ArrayList<>();	
	    for (int i = 0; i < strings.size(); i++) {
	       
	        if (i == 0) {
	            big = Integer.parseInt(strings.get(i));
	        } 
	        else if (i == 1) {
	            medium = Integer.parseInt(strings.get(i));
	        }
	        else if (i == 2) {
	            small = Integer.parseInt(strings.get(i));
	        } 
	        else {
	            planSignUp.add(Integer.parseInt(strings.get(i)));
	        }
	    }
	    Params params = new Params();
	    params.setParams(big, medium, small, planSignUp);
	    return params;
	}
}
class Params implements IParams {       //定义大中小班的属性
    private int big;
    private int medium;
    private int small;
    private ArrayList<Integer> planSignUp;
    public void setParams(int big, int medium, int small, ArrayList<Integer> planSignUp) {
        this.big = big;		 //给大班名额赋值
        this.medium = medium;	//给中班名额赋值
        this.small = small;	//给小班名额赋值
        this.planSignUp = planSignUp;
    }
    public int getBig() {         
        return big;
    }
    public int getMedium() {
        return medium;
    }
    public int getSmall() {
        return small;
    }
    public ArrayList<Integer> getPlanSignUp() {
        return planSignUp;
    }
}
public class SchoolSystem implements ISignUp {
    public int big;
    public int medium;
    public int small;
    SchoolSystem(int big, int medium, int small) {
        this.big = big;
        this.medium = medium;
        this.small = small;
    } 
    public SchoolSystem() {}
	static Scanner s = new  Scanner(System.in);     
    static List<Object> list = new ArrayList<>();
    public static void main(String[] args) throws Exception {
        IParams params = ISignUp.parse();        //SchoolSystem.parse();
        SchoolSystem sc = new SchoolSystem(params.getBig(), params.getMedium(), params.getSmall());
        list.add(null);
        ArrayList<Integer> plan = params.getPlanSignUp();
        for (int i=0;i<plan.size(); i++) {
           list.add(sc.addStudent(plan.get(i)));
        }
        sc.print();		
        }
    public void print() {
    	  System.out.println(list);       	
    }
    public boolean addStudent(int stuType) {
            if (stuType == 1) {		//当值等于1且大班名额小于0时，结果返回false
                if (big-- < 0) {
                    return false;
                }
                
            }
            else if (stuType == 2) {		//当值等于2且中班名额小于0时，结果返回false
                if (medium-- < 0) {
                    return false;
                }
            }
            
            else if (stuType == 3) {		//当值等于3且小班名额小于0时，结果返回false
                if (small-- < 0) {
                    return false;
                }
                return true;
            }
            return false;
		}
    public String[]  parse(String a1) {
        a1 = a1.substring(1,a1.length()-1);
        String ss[] = a1.split(",");
        return ss;
    }
}











































